extends Area2D

export (PackedScene) var bullet_scene
export (Curve) var opacity_curve

signal done_shooting

var shots_total # how many bullets the powerup gives you in total
var shots_left  # how many bullets are remaining
var speed = -2000 # the speed of the bullets
var opacity:float = 1 # how visible the bullet is (it gets less visible as u run out of bullets)
var negatizer:float # the amount the opacity will decrease each bullet

func spawn():
	randomize()

	var bullet = bullet_scene.instance()
	bullet.position = $barrel.global_position
	bullet.rot = $barrel.global_rotation
	bullet.rotation = $barrel.global_rotation+.5*PI
	bullet.speed = speed
#	opacity -= negatizer
#	make the opacity decrease
#	first wait ~j0.8s between each enemy, then slowly as u get to the maximum
#	difficulty height, lower the wait time to ~0.3 seconds
	opacity = opacity_curve.interpolate((shots_total-shots_left)/float(shots_total))
	bullet.modulate.a = opacity
	print((shots_total-shots_left)/float(shots_total), " ", opacity," ",bullet.modulate.a)
	utils.main.add_child(bullet)

	yield(get_tree().create_timer(0.3), "timeout")
	if shots_left > 0:
		if get_tree().paused:
			yield(utils, "unpaused")
		shots_left -= 1
		spawn()
	else:
		emit_signal("done_shooting")
		print("done")
		queue_free()


func shoot(amount):
	opacity = 1
#	negatizer = 0# opacity/(amount+1)
	shots_total = amount
	shots_left = amount
	spawn()
