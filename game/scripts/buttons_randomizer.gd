extends VBoxContainer

# to move around the buttons (its the margins ontop of the buttons actually)
export (Array, NodePath) var buttons
# this is the real buttons.
export (Array, NodePath) onready var buttons_actual

func _ready() -> void:
	randomize()
	for button in buttons:
		var margin = int(round(rand_range(0, 500)))
		var side = "margin_right" if randf()>.5 else "margin_left"
		get_node(button).set("custom_constants/"+side, margin)

	for i in buttons_actual.size():
		buttons_actual[i] = get_node(buttons_actual[i])

	utils.connect("gameover",self,"_on_gameover")


# draw the lines between each menu piece
# this way, the user will know that the menu is scrollable
func _draw() -> void:
	if name=="buttons_main":
		for i in buttons_actual.size():
			if i+1 < buttons_actual.size():
				var button = buttons_actual[i]
				var pos = button.rect_global_position
				var pos2 = buttons_actual[i+1].rect_global_position # the pos of the next item
#				draw_circle(pos, 10, Color.red)
				var size = button.rect_size
				var size2 = buttons_actual[i+1].rect_size # the size of the next item
				var vertical_offset = 10 # the amount of white space between the item and the curve
#				if name=="buttons_main":
#					horizontal_offset = -35
#				elif name=="buttons_gameover":
#					horizontal_offset=-140

#				p0
				var start_pos = Vector2(pos.x-size.x/2,pos.y+size.y+vertical_offset)
#				p3
				var end_pos = Vector2(pos2.x-size2.x/2,pos2.y-vertical_offset)#buttons_actual[i+1].rect_global_position + Vector2(-35,-10)#Vector2(pos2.x-size2.x/2,pos2.y+size2.y)
#				draw_circle(start_pos, 10, Color.green)
#				draw_circle(end_pos, 10, Color.blue)
				var pull = Vector2(0, 100)
				var p1 = start_pos + pull
				var p2 = end_pos - pull
				var points : PoolVector2Array
				for t in 100:
					t /= 100.0
					var point = pow((1-t),3)*start_pos+3*pow((1-t),2)*t*p1+3*(1-t)*pow(t,2)*p2+pow(t,3)*end_pos
					points.append(point)

				draw_polyline(points, Color("777777"), 10, false)
#				draw_line(start_pos, end_pos, Color("777777"), 10, true)
