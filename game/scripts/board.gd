extends Control
var show_menu = true
func _ready() -> void:
	transition.connect("reloaded_scene", self, "_on_reloaded_scene")
	scores.reset()
	utils.camera = $player/camera
	print("show menu", show_menu)
	if not show_menu:
		$menus/menu_main.hide()
	if show_menu:
		$joystick/joystick.hide()
		$player.hide()
		$ui/hud.hide()


func _on_reloaded_scene(show_menu):
	if not show_menu:
		start()

var started = false
func start():
	"""
	this sets up everything that u need to start the game
	"""
	print("start")
	started = true
	$joystick/joystick.show()
	if scores.max_height > 20:
		$max_height.show()

#	start up the scene
	$player.start()
	$blocks.start()
	$floor.start()
	$powerups.start()
	$ui/hud.show()
	$ui/hud/scores/score/score.start()


# screen drag
var speed
var pressed=false
var last_press=false
func _input(event: InputEvent) -> void:
	if not started:
		if event is InputEventScreenDrag:
			speed = -event.relative.y*10
#			print(speed)
#			get the menu to test the screen edges
			var menu:Container
			for node in get_tree().get_nodes_in_group("menu"):
				if node.visible:
					menu = node
			if menu:
#				if u reach the top, only allow downwards scroll
				if menu.rect_global_position.y-100 >= utils.top.y:
					print("at top")
					if event.relative.y > 0:
						print("scrolling up")
						return
#				if u reach the bottom, only allow upwards scroll
				if menu.rect_global_position.y+menu.rect_size.y+250 <= utils.bottom.y:
					if event.relative.y < 0:
						return
#				move the camera
				$player/camera.offset.y -= event.relative.y
#				disable buttons while scrolling so that u dont by mistake press
#				a button while scrolling
#				however, only after a offset,
#				cuz then its impossible to click things on certain phones
				if event.relative.y > 5 or event.relative.y < -5:
					for button in get_tree().get_nodes_in_group("button"):
						if button.disabled:
							break
						button.disabled = true
#		if event is InputEventScreenTouch:
#			pressed = event.pressed
#			if not pressed and last_press:
#				if speed:
#						$tween.interpolate_property($player/camera, "offset", $player/camera.offset, $player/camera.offset+Vector2(0,speed),0.5,Tween.TRANS_CUBIC,Tween.EASE_OUT)
#	$tween.start()
#			last_press = pressed
#		reenable the disabled buttons so that just tapping a button presses it
		else:
			for button in get_tree().get_nodes_in_group("button"):
				if not button.disabled:
					break
				button.disabled = false
