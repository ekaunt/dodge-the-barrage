extends VBoxContainer

var max_height = 0
var max_heights : Array

onready var key = "_MAX_HEIGHT"
onready var start_pos = $"/root/main/board/player".position.y

func _save(savegame : Resource):
	if scores.height > max_height:
		max_height = scores.height
		max_heights[gamemode.gamemode_names.find(gamemode.current_gamemode.name())] = max_height
	var i = 0
	for gm in gamemode.gamemode_names:
		print("gm "+gm)
		key = gm + "_MAX_HEIGHT"
#		if savegame.data.has(key):
		savegame.data[key] = max_heights[i]
		i += 1

func _load(savegame : Resource):
	max_heights.clear()
	for gm in gamemode.gamemode_names:
		var i = gamemode.gamemode_names.find(gm)
		print("gm "+gm, gamemode.current_gamemode.name())
		max_heights.append(0)
		max_heights[max_heights.size()-1] = 0
		key = gm + "_MAX_HEIGHT"
		print(key,savegame.data.has(key))
		if savegame.data.has(key):
			print(i, " ", max_heights[i]," ",key," ",savegame.data[key])
			max_heights[i] = savegame.data[key]
			print(i, " ", max_heights[i])
			if gamemode.get_gamemode(gm).is_current_gamemode():
				print("currentgamemoedis ",gamemode.current_gamemode.name())
				max_height = max_heights[i]
				scores.max_height = max_height
				rect_position.y = -max_height*100+start_pos
				$label.text = str(max_height)+" - "+$label.text
			print("maxheight ",max_height)
	print(max_heights)
