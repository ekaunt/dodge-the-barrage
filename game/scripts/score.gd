extends Label

var score:int
var start_pos
var last_pos = 0
var started = false
var position

func start():
	start_pos = $"/root/main/board/player".position.y
	started = true
	scores.connect("score_changed", self, "_update_score")

func _process(delta: float) -> void:
	if started:
		position = ceil(abs($"/root/main/board/player".position.y - start_pos)/100)
		score = position - last_pos
#		dont increase score if going down
		if scores.score + score >= scores.score:
			scores.score += score
			scores.height +=score
		last_pos = position

func _update_score(new_score):
	text = str(new_score)
