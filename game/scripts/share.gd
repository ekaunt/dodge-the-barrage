tool
extends "res://scripts/button.gd"

var share = null # our share singleton instance

func _ready():
	# initialize the share singleton if it exists
	if Engine.has_singleton("GodotShare"):
		share = Engine.get_singleton("GodotShare")

func _pressed() -> void:
	if share:
		share.shareText(tr("SHARE1"), tr("SHARE2"), tr("SHARE3"))
