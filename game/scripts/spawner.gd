extends Node
class_name Spawner

onready var block_scene = preload("res://scenes/block.tscn")
var speed = 100
var offset = 0
var block_size = 0

func _ready():
	var blockscene = block_scene.instance()
	offset = blockscene.get_node("sprite").texture.get_height()/2 * blockscene.get_node("sprite").scale.y
	block_size = blockscene.get_node("sprite").texture.get_height() * blockscene.get_node("sprite").scale.y
