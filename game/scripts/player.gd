extends KinematicBody2D

signal updated

export var friction = 1


var alive = false# for the stupid bug that doesnt let me spawn if i go fast when i die
var invincible = true # export in this case means 'public'
var factor = Vector2()
var last_output = Vector2()
var acc = Vector2()
var vel = Vector2()
var default_color = modulate
var trail_length = 0 setget _trail_update
var original_length = 50
var forwards : bool

onready var max_speed = gamemode.current_gamemode.max_speed
onready var gun_scene = preload("res://scenes/gun.tscn")
onready var trail = $trail/trail
onready var offset = $sprite.texture.get_height()/2 * $sprite.scale.y
#onready var chromatic_aberration = $"../ui/chromatic_aberration"

func start():
	print("player start")
	invincible = false
	alive = true
	match gamemode.current_gamemode:
		gamemode.HYPER:
			utils.joystick.dead_zone = 0.99

func _physics_process(delta: float) -> void:
#	movement:
	if alive:
		if utils.joystick.is_working:
			factor = utils.joystick.output
			if not forwards:
				factor = -factor
#			if the gamemode is hyper, remember the last position the joystick
#			was at, so that i can keep the acceleration going, even once my
#			hand goes off of the joystick
			if gamemode.current_gamemode == gamemode.HYPER:
				if factor != Vector2.ZERO:
					last_output = factor
			acc = factor * max_speed
			rotation = factor.angle() +PI*.5
		else:
			match gamemode.current_gamemode:
				gamemode.SUPER:
					acc = Vector2()
				gamemode.HYPER:
					acc = last_output * max_speed

		acc += vel * -friction
		vel += (acc*4) * delta
		move_and_slide(vel)
#		$"/root/main/board/ui/m/score2".text = str(-vel.ceil().y)


	# keep trail connected to me
	trail.add_point(global_position)
	trail.remove_point(0)

#	chromatically aberrate the screen when going really fast
#	if thrust > 600:
#		chromatic_aberration.amount = ((vel.ceil().y-600)*(0.5-0))/((1000-600))+0

	# die if u hit the bottom of the screen
	if position.y - offset > utils.bottom.y and alive:
		die(true)


func die(die_anyway=false):
	"""
	all the logic needed to kill the player
	"""
	if $shape and (not invincible or die_anyway):
		gamemode.current_gamemode.set_highscore(scores.highscore)
		gamemode.current_gamemode.save_highscore()
		settings.save()
		utils.emit_signal("gameover")
		alive = false
#		$gun.queue_free()
#		$gun_right.queue_free()
#		$gun_left.queue_free()
		$shape.queue_free()
		utils.joystick.hide()
		$"../../board".started = false
		hide()
		$"../ui/hud".hide()

#		show the gameover screen
#		and put the main menu screen above it, out of view
		$"../menus/menu_gameover".rect_global_position = utils.top
		$"../menus/menu_gameover".show()

		#show an advertisement 1/15x
#		if randi()%15==0:
#			utils.main.show_interstitial()


func fast(time):
	"""
	make the player go fast and become invincible
	"""
	print(max_speed)
#	match gamemode.current_gamemode:
#		gamemode.SUPER:
#			var original_thrust = 600.0
#			self.thrust = 1000.0
#		gamemode.HYPER:
#			var original_thrust = 800.0
#			self.thrust = 1300.0
	max_speed = gamemode.current_gamemode.max_speed_boost
	invincible = true
	print(max_speed)
	#	trail
	trail_length = original_length
	while trail.get_point_count() < trail_length:
		trail.add_point(global_position)
#	if this is the second time hitting a fast powerup while already having one,
#	we need to first stop the tween before restarting it or else it doesnt work
	$tween.stop_all()
	$tween.interpolate_property(self, "trail_length",
		original_length, 0, time, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$tween.start()
	print("fast")
	yield($tween, "tween_completed")
	print("fast_done")
#	$tween.interpolate_property(chromatic_aberration, "amount", chromatic_aberration.amount, 0, 0.5, Tween.TRANS_LINEAR, Tween.EASE_OUT)
#	$tween.start()
	trail_length = original_length
	invincible = false
	max_speed = gamemode.current_gamemode.max_speed
	print(max_speed)


func shoot(amount):
	"""
	shoot a barrage of bullets for time seconds
	params:
		amount: the amount of bullets to shoot
	"""
	var gun = gun_scene.instance()
	add_child(gun)
#	make the gun stop shooting if the player dies
	utils.connect("gameover", gun, "queue_free")
	gun.shoot(amount)


func triple_shot(amount):
	"""
	enables 2 extra blasters for amount seconds
	params:
		amount: the amount of bullets to shoot
	"""
	var gun = gun_scene.instance()
	add_child(gun)
	utils.connect("gameover", gun, "queue_free")
	gun.shoot(amount)
	var gun_right = gun_scene.instance()
	add_child(gun_right)
	utils.connect("gameover", gun_right, "queue_free")
	gun_right.shoot(amount)
	gun_right.position = Vector2(-16, -18)
	gun_right.rotation_degrees = -140
	var gun_left = gun_scene.instance()
	add_child(gun_left)
	utils.connect("gameover", gun_left, "queue_free")
	gun_left.shoot(amount)
	gun_left.position = Vector2(16, -18)
	gun_left.rotation_degrees = -50

func bomb(type):
	"""
	lay a bomb where u are currently
	params:
		type: the bomb type to set the bomb to
	"""
	utils.spawn_bomb(type, global_position)

func _trail_update(new_trailsize):
	trail_length = new_trailsize
	while trail.get_point_count() > trail_length:
		trail.remove_point(0)
