extends Polygon2D

# since were gonna do many things related to the size of enemy blocks,
# we should set a quick and easy way to get its size
var block_size = (64*1.5)

func _ready() -> void:
#	set the polygon shape dynamically
	for block in get_viewport_rect().size.x / block_size:
		var p = polygon
		var pos = block*block_size
		p.append(Vector2(pos, 1000))
#		p.append(Vector2(pos+block_size, 1000))
		set_polygon(p)
	print(polygon)


# the position of the mouse
var pos:Vector2
# the closest point in the polygon to where the mouse clicked
var closest_point=polygon[0]
func _input(event: InputEvent) -> void:
	if Input.is_action_just_pressed("press"):
		pos = get_global_mouse_position()
		closest_point=polygon[0]
#		calculate the closest_point
		var i = 0
		var index = i
		for point in polygon:
#			test if the point new point is closer than the last point tested12
			if (point-pos).length() < (closest_point-pos).length():
				closest_point = point
				index = i
			update()
			i += 1
		raise_point(index)

func raise_point(index):
	var nearest_point = polygon[index]
	var other_point = polygon[index-1]

	var p = polygon
	p.insert(index, nearest_point)
	p.insert(index-1, other_point)
	set_polygon(p)

	polygon[index]		= Vector2(closest_point.x,		closest_point.y-block_size)
	polygon[index-2]	= Vector2(polygon[index-2].x,	polygon[index-2].y-block_size)
	closest_point=polygon[index]

	update()


func _draw() -> void:
	for point in polygon:
		draw_circle(point, 10, Color.coral)
	draw_line(pos, closest_point, Color.red, 10)

