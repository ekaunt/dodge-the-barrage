extends HBoxContainer

export (NodePath) var joystick
export (Array, NodePath) var spawners

func _notification(what: int):
	if what == MainLoop.NOTIFICATION_WM_FOCUS_OUT or \
	   what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
		pause()

func pause():
	if $"/root/main/board".started:
		rect_position = utils.top
		show()
		$buttons/score._on_paused()
		$buttons/highscore/highscore._on_paused()
		get_node(joystick).hide()
		for spawner in spawners:
			get_node(spawner).stop()
		get_tree().paused = true
