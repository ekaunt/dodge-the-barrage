extends Area2D

var speed
var rot

func _process(delta: float) -> void:
	position += Vector2(0,speed*delta).rotated(rot+.5*PI)

func _on_bullet_body_entered(body: PhysicsBody2D) -> void:
	if body is block:
		body.destroy()
		queue_free()

func _on_visibility_notifier_screen_exited() -> void:
	queue_free()
