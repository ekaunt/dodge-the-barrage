extends MarginContainer

export(float) var viewport_height_percentage := 1.5

func _ready():
	var rect = get_viewport().get_visible_rect()
	rect_min_size.y = rect.size.y * viewport_height_percentage

