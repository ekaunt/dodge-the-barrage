extends Node

signal score_changed

var score = 0 setget update_score
var max_height = 0
var highscore = 0
var height = 0

func _ready():
	update_score(score)

func update_score(new_score):
	score = new_score
	if score > highscore:
		highscore = score
	emit_signal('score_changed', score)

func reset():
	self.score = 0
	self.height = 0
	self.highscore = gamemode.current_gamemode.highscore
