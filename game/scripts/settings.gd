extends Node
"""
saves and loads savegame files
Each node is responsible for finding itself in the save_game dict so
saves dont rely on the nodes path or their resource file
@author GDQuest
"""

onready var _savegame = preload("res://scripts/savegame.gd")
var SAVE_FOLDER = "user://"
var SAVE_NAME_TEMPLATE = "save_%03d.tres"

# Passes a savegame resource to all nodes, to save data from
# and writes to the disk
# @param int id the savegame slot to save to
func save(id:int = 0):
	var savegame = _savegame.new()
	savegame.game_version = ProjectSettings.get_setting("application/config/version")
	for node in get_tree().get_nodes_in_group("save"):
		node._save(savegame)

	var directory : Directory = Directory.new()
	if not directory.dir_exists(SAVE_FOLDER):
		directory.make_dir_recursive(SAVE_FOLDER)

	var save_path = SAVE_FOLDER.plus_file(SAVE_NAME_TEMPLATE % id)
	var error : int = ResourceSaver.save(save_path, savegame)
	if error != OK:
		print("there was an error writing ", id, " to ", save_path)
	print(savegame.data)

# Reads a saved game from disk, and delegates loading to the individual nodes
# @param int id the savegame slot to save to
func load(id:int = 0):
	var save_file_path = SAVE_FOLDER.plus_file(SAVE_NAME_TEMPLATE % id)
	var file:File = File.new()
	if not file.file_exists(save_file_path):
		print("save file ", save_file_path, " dosnt exist")
		return

	var savegame:Resource = load(save_file_path)
	for node in get_tree().get_nodes_in_group("save"):
		node._load(savegame)
	print(savegame.data)









##var muted : bool
#var filepath = "user://settings"
#var file = File.new()
#var settings:Dictionary = {
#	muted: false,
#	direction: "forwards",
#}
#
#func _ready() -> void:
#	print(settings)
#	save(get_sound())
#	print(settings)
#
#func save(muted:bool):
#	AudioServer.set_bus_mute(AudioServer.get_bus_index('Master'), muted)
#	file.open(filepath, File.WRITE)
#	file.store_var(muted)
#	file.close()
#	self.muted = muted
#
#func get_sound():
#	file.open(filepath, File.READ)
#	var muted = file.get_var()
#	file.close()
#	if typeof(muted) == TYPE_BOOL:
#		return muted
#	else:
#		return false
