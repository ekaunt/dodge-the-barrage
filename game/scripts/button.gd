tool
extends TextureButton
class_name button

export (Texture) var image setget _update

func _update(_image):
	image = _image
	$image.texture = image

func _on_button_pressed() -> void:
	$sound.play()
