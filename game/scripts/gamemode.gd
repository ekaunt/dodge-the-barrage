extends Node

onready var gamemode_manager = load('res://scripts/gamemode_manager.gd')

#set up all gamemodes - everything in uppercase so that a simple rename can rename every reference of it
onready var SUPER	= gamemode_manager.new("SUPER", 600.0)
onready var HYPER	= gamemode_manager.new("HYPER", 800.0)

onready var gamemodes	=	[SUPER, HYPER]
onready var gamemode_names=	["SUPER", "HYPER"]

#the current gamemode. will be shared through all objects.
onready var current_gamemode = SUPER

func set_gamemode(gamemode_string):
	"""
	sets the gamemode object by its name.
	args:
		gamemode_string: The string representation of the gamemode. Must be in all caps.
	"""
	for gamemode in gamemodes:
		if gamemode.name().to_lower() == gamemode_string.to_lower():
			current_gamemode = gamemode


func get_gamemode(gamemode_string):
	"""
	Get the gamemode object by its name.
	args:
		gamemode_string: The string representation of the gamemode. Must be in all caps.
	returns:
		The specified gamemode.
	"""
	print(gamemode_string)
	for gamemode in gamemodes:
		if gamemode.name().to_lower() == gamemode_string.to_lower():
			return gamemode
	for gamemode in gamemode_names:
		if gamemode.to_lower() == gamemode_string.to_lower():
			return gamemode
	return null
