tool
extends RigidBody2D

# all powerup types
enum {fast, shoot, triple_shot, bomb, super_bomb}

var powerups = {
	fast: preload("res://art/powerups/pu_fast.png"),
	shoot: preload("res://art/powerups/pu_shot.png"),
	triple_shot: preload("res://art/powerups/pu_shot_triple.png"),
	bomb: preload("res://art/powerups/bomb.png"),
	super_bomb: preload("res://art/powerups/bomb_super.png"),
}
var weights = [
	0.2,
	0.3,
	0.05,
	0.2,
	0.05,
]

# the powerup type that this current powerup is. its set by the spawner
var type

func _ready():
	$square/icon.texture = powerups[type]

func powerup(player):
	match type:
		fast:
			player.fast(3)
		shoot:
			player.shoot(10)
		triple_shot:
			player.triple_shot(10)
		bomb:
			player.bomb("regular")
		super_bomb:
			player.bomb("super")

func _on_body_entered(body):
	if body.name == "player":
		powerup(body)
		queue_free()
	if body.is_in_group("floor"):
		body.destroy()
		queue_free()


func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()
