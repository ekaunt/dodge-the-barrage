extends Sprite

enum {regular, super}
var type

func _ready():
	material = material.duplicate()
#	$sound.play()

func get_scaled():
	return get_rect().size * scale

var time = 0
var duration = 1

var _scale = 15
#var _intensity = 2.5

func _process(delta):
	if time < duration:
#		if the game gets paused, we want to pause the explosion.
#		not sure why the engine doesnt pause it for me, but this is how it is.
		if get_tree().paused:
			var current_time = time
			yield(utils, "unpaused")
			time = current_time
#		print(time)
		var progress = time / duration
		scale =  (Vector2(_scale,_scale) * progress).linear_interpolate(Vector2(_scale,_scale), progress)
		#material.set_shader_param("amplitude", Vector2(0.15,0.35)) #* ( 1 - progress ))
		material.set_shader_param("bias", 1-progress)
		material.set_shader_param("intensity", ( 1 - progress ))
		time += delta
	else:
		queue_free()


func _on_body_body_entered(body):
	if body is block:
		body.destroy()
		if type == super:
			utils.spawn_bomb("regular", body.global_position)
