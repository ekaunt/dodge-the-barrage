extends Spawner

onready var powerups = preload("res://scenes/powerup.tscn")

func start():
	yield(get_tree().create_timer(0.2), "timeout")
	speed = 300
	var powerup = powerups.instance()
	offset = powerup.get_node("square").texture.get_height()/2 * powerup.get_node("square").scale.y
	stopped = false
	spawn()

var stopped = false
func stop():
	stopped = true

func spawn():
	randomize()

	var powerup = powerups.instance()
	powerup.position.x = rand_range(offset, utils.right)#utils.bottom.x)
	powerup.position.y = utils.top.y - offset
	# powerup.apply_impulse(Vector2(),Vector2(0, speed).rotated(rand_range(PI*-0.2,PI*0.2)))
	powerup.linear_velocity = Vector2(0, speed).rotated(rand_range(PI*-0.2,PI*0.2))
#	evenly choose a random powerup type
	powerup.type = powerup.powerups.keys()[utils.choose_weighted(powerup.weights)]
	$container.add_child(powerup)

	yield(get_tree().create_timer(3), "timeout")
	if not stopped:
		spawn()
