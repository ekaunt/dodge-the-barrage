extends KinematicBody2D

onready var offset = $shape.shape.extents.y

func _process(delta):
	position.y = offset + utils.top.y
