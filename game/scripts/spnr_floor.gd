extends Spawner

export (Curve) var speed_curve
#export var maximum_speed_height = 50
var gameover = false

func _ready() -> void:
	utils.floors = self
	utils.connect("gameover", self, "_on_gameover")
func _on_gameover():
	gameover = true

var started = false
func start():
	# the speed the floor rises
	match gamemode.current_gamemode:
		gamemode.SUPER:
			speed = -200
		gamemode.HYPER:
			speed = -500
	yield(get_tree().create_timer(1), "timeout")
	started = true
	spawn()

func stop():
	started = false
	for flr in get_tree().get_nodes_in_group("floor"):
		flr.linear_velocity = Vector2(0,0)

func _process(delta: float) -> void:
	# this is to make sure that new floor layers only spawn
	# only if the lowest layer is in view. this way only the
	# amount of layers that u can see can spawn so that there
	# arent a bunch of extra layers loaded!
	if started:
		if get_tree().get_nodes_in_group("bottom").size() == 0:
			spawn()
#		print(get_tree().get_nodes_in_group("bottom").size())
		for flr in get_tree().get_nodes_in_group("bottom"):
#			speed = -speed_curve.interpolate(scores.height/maximum_speed_height)
			if flr.position.y <= utils.bottom.y and not gameover:
				if get_tree().paused:
					yield(utils, "unpaused")
				spawn()
				break

func spawn():
	for node in get_tree().get_nodes_in_group("second_to_bottom"):
		node.remove_from_group("second_to_bottom")
	for node in get_tree().get_nodes_in_group("bottom"): # ← check
#		only the bottom layer is in the "bottom" group
#		make the bottom layer the second to bottom layer,
#		since were spawning a new layer in now
		node.remove_from_group("bottom")
		node.add_to_group("second_to_bottom")
#		remove current second to bottom layer to make space for the new ones


#	add a new layer to the floor
	var second_to_bottom = get_tree().get_nodes_in_group("second_to_bottom")
	for f in range(utils.right / block_size + 1):
		var flr = block_scene.instance()
		var position
		if second_to_bottom:
			position = second_to_bottom[0].position.y
		else:
			position = utils.bottom.y
		add_piece(flr, Vector2(f * block_size, position+block_size-10))
#	top means top-left corner, so im just using the left value
#	abs it cuz the left x is a negative value
#	for f in range(abs(get_viewport().get_visible_rect().position.x) /  block_size + 1):
#		var flr = block_scene.instance()
#		flr.position = Vector2(-f * block_size, utils.bottom.y+offset-3)
#		add_piece(flr)


func add_piece(flr, position):
	if not flr:
		flr = block_scene.instance()

#	TODO: make position snap to grid
	flr.position = position
	# since this is the same block.tscn as the falling blocks,
	# and since the floors collision is different than the blocks,
	# i need to change what the floor can collide with
	flr.set_collision_layer_bit(1, false)
	flr.set_collision_layer_bit(2, true)
	flr.set_collision_mask_bit(1, false)
	flr.set_collision_mask_bit(3, false)

	flr.linear_velocity = Vector2(0,speed)

	flr.type = flr.FLR
	flr.add_to_group("bottom")
	flr.add_to_group("floor")
#	flr.connect("bump_floor", self, "_bump")
	$container.add_child(flr)

#func _bump(Block):
#	Block.linear_velocity = Vector2(0, speed)
#	Floor.linear_velocity = Vector2(0, speed)
