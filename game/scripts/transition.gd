extends CanvasLayer

signal reloaded_scene
signal changed_scene
signal opacity_changed
# STORE THE SCENE PATH
var path = "res://scenes/board.tscn"
var opacity setget _update_opacity
var show_menu = false

# PUBLIC FUNCTION. CALLED WHENEVER YOU WANT TO CHANGE SCENE
func fade_to(show_menu:bool):
	self.show_menu = show_menu
	utils.floors.stop()
	$fade.show()
	$anim.play("fade") # play the transition animation
	$tween.interpolate_property(utils.camera, "offset", utils.camera.offset, utils.camera.offset+Vector2(0,1920*2), 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$tween.start()
	yield($anim, "animation_finished")
	$fade.hide()

# PRIVATE FUNCTION. CALLED AT THE MIDDLE OF THE TRANSITION ANIMATION
func reload_scene():
	if path != "":
#		get the particle offset so that it doesnt just to 0 when the scene reloads
#		var current_offset = particles.scroll_offset
#		print("done", current_offset, particles.scroll_offset)

#		save all the settings
		print("saving")
		settings.save()

#		reload the scene
		var scene = load(path).instance()
		scene.show_menu = show_menu
		var board = utils.main.get_node("board")
		board.queue_free()
		yield(board, "tree_exited")
		utils.main.add_child(scene)

#		load all settings into the new scene
		print("loading")
		settings.load()

#		yield(get_tree(), "idle_frame")
#		yield(get_tree(), "idle_frame")
#		print("waited", current_offset, particles.scroll_offset)
#		emit_signal("changed_scene", current_offset)

func emit():
	emit_signal("reloaded_scene", show_menu)

func _update_opacity(new_value):
	opacity = new_value
	utils.main.modulate.a = opacity
	emit_signal("opacity_changed", opacity)
