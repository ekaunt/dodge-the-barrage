extends Node2D

signal gameover
signal unpaused

onready var bomb_scene = preload("res://scenes/bomb.tscn")
# the main game scene where everyhing happens.
var main:Node
# the main camera that follows the player
var camera:Camera2D
# top left corner
var top:Vector2
# bottom right corner
var bottom:Vector2
# the right side of the playable area (usually 1080, but maybe smaller)
var right:int
# everything having to do about uthe state of the joystick
var joystick
# the floor root node
var floors:Node

func _ready() -> void:
	var right_edge = get_viewport_rect().end.x
	right = 1080 if right_edge > 1080 else right_edge
	set_positions()
func _process(delta):
	set_positions()


func set_positions():
	"""
	figure out the top right and bottom left position of the cameras view
	@author internet-man
	"""
	# Get the canvas transform
	var ctrans = get_canvas_transform()

	# The canvas transform applies to everything drawn,
	# so scrolling right offsets things to the left, hence the '-' to get the world position.
	# Same with zoom so we divide by the scale.
	top = -ctrans.get_origin() / ctrans.get_scale()

	# The maximum edge is obtained by adding the rectangle size.
	# Because it's a size it's only affected by zoom, so divide by scale too.
	var view_size = get_viewport_rect().size / ctrans.get_scale()
	bottom = top + view_size


func choose(choices):
	randomize()
	var rand_index = randi() % choices.size()
	return choices[rand_index]

func choose_weighted(weights):
	var sum = 0
	for w in weights:
		sum += w
	var rnd = randf() * sum
	var i = 0
	for w in weights:
		rnd -= w
		if rnd < 0:
			return i
		i += 1


func spawn_bomb(type, pos):
	var bomb = bomb_scene.instance()
	bomb.global_position = pos
	match type:
		"regular": bomb.type = bomb.regular
		"super": bomb.type = bomb.super
	$"/root/main/board".add_child(bomb)
