extends RigidBody2D
class_name block

signal bump(Block, Block)
signal bump_floor(block_position, Floor, direction)
enum { BLC, FLR }
var type = BLC
onready var offset = $sprite.texture.get_height()/2 * $sprite.scale.y

# warning-ignore:unused_argument
func _process(delta):
	if (position.y - offset > utils.bottom.y and type != FLR) or position.y + offset < utils.top.y:
		queue_free()
#	make the block detect if its about to hit the floor
	if $ray_down.enabled:
		if $ray_down.is_colliding():
#			print(position)
			emit_signal("bump_floor", position, $ray_down.get_collider(), "down")
			queue_free()
	if $ray_right.enabled:
		if $ray_right.is_colliding():
			$ray_down.enabled = false
##			print(position)
#			emit_signal("bump_floor", position, $ray_right.get_collider(), "right")
#			queue_free()
	if $ray_left.enabled:
		if $ray_left.is_colliding():
			$ray_down.enabled = false
##			print(position)
#			emit_signal("bump_floor", position, $ray_left.get_collider(), "left")
#			queue_free()


func _on_body_entered(body):
	if body.name == "player":
		body.die()
		destroy()

#	elif type == FLR and body.type == BLC:
#		emit_signal("bump_floor", self, body)

	elif type == BLC:
		emit_signal("bump", self, body)


func destroy() -> void:
		$shape.queue_free()
		$sprite.hide()
		$particles.emitting = true
		yield(get_tree().create_timer($particles.lifetime), "timeout")
		match type:
			BLC:
				scores.score += 10
			FLR:
				scores.score += 1
		queue_free()


var on_screen = false
func _on_block_screen_entered() -> void:
	on_screen = true
func _on_block_screen_exited() -> void:
	on_screen = false
