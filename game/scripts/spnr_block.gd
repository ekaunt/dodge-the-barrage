extends Spawner

export (Curve) var waiting_time_curve
export var maximum_difficulty_height = 300
onready var bump_scene = preload('res://scenes/bump.tscn')
onready var spnr_floor = $"../floor"
var stopped = false

func start():
	yield(get_tree().create_timer(0.2), "timeout")
	randomize()
	speed = 500
	stopped = false
	spawn(Vector2(rand_range(offset, utils.right), utils.top.y - offset))

func stop():
	stopped = true

#func _input(event: InputEvent) -> void:
#	if event is InputEventMouseButton:
#		spawn(event.position)
#		print(event.position)

func spawn(pos):
	var block = block_scene.instance()
	block.position.x = pos.x
	block.position.y = pos.y
	block.linear_velocity = Vector2(0, speed).rotated(rand_range(PI*-0.2,PI*0.2))
	block.get_node("ray_down").enabled = true
	block.get_node("ray_right").enabled = true
	block.get_node("ray_left").enabled = true
	block.connect("bump", self, "_bump")
	block.connect("bump_floor", self, "_bump_floor")
	$container.add_child(block)

#	make the difficluty ramp up
#	first wait ~0.8s between each enemy, then slowly as u get to the maximum
#	difficulty height, lower the wait time to ~0.3 seconds
	var waiting_time = waiting_time_curve.interpolate(scores.height/maximum_difficulty_height)
	yield(get_tree().create_timer(waiting_time), "timeout")
	if not stopped:
		if get_tree().paused:
			yield(utils, "unpaused")
		spawn(Vector2(rand_range(offset, utils.right), utils.top.y - offset))

func _bump(block, body):
	"""
	create a little particle effect whenever a block hits something
	"""
	var bump = bump_scene.instance()
	var rot = block.global_position.angle_to(body.global_position)
#	i want the effect to show up where the block hit something,
#	not in the center of the block.
	bump.global_position = block.global_position + \
		(Vector2(0,block.get_node("sprite").texture.get_height()/2).rotated(rot))
	bump.emitting = true
	add_child(bump)
	yield(get_tree().create_timer(bump.lifetime), "timeout")
	bump.queue_free()

func _bump_floor(block_position, Floor, direction):
	print(direction)
#	snap the block to the grid
	match direction:
		"down":
			block_position.x = Floor.position.x
			block_position.y = Floor.position.y - block_size
#		"left":
#			block_position.y = Floor.position.y
#			block_position.x = Floor.position.x + block_size
#			print("left")
#		"right":
#			block_position.y = Floor.position.y
#			block_position.x = Floor.position.x - block_size
#		"left" or "right":
#			block_position.y = Floor.position.y
#			print("left or right")
			spnr_floor.add_piece(false, block_position)
